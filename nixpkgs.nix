fetchTarball {
  # NixOS 19.03 02019-09-13
  url = "https://github.com/NixOS/nixpkgs-channels/archive/8a30e242181410931bcd0384f7147b6f1ce286a2.tar.gz";
  sha256 = "0574zwcgy3pqjcxli4948sd3sy6h0qw6fvsm4r530gqj41gpwf6b";
}
