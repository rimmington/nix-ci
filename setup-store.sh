set -euo pipefail
set -x

if [ -e .nix-store.tar ]; then
    tar -xpf .nix-store.tar
    rm .nix-store.tar
fi

# fetchTarball is broken with chroot stores
# https://github.com/NixOS/nix/issues/2405
# so we fetch to /nix/store and then copy to cache
store=$(pwd)/.nix-store
nixpkgs=$(nix eval --raw '(import ./nixpkgs.nix)')
nix copy --to local?root=$store $nixpkgs
export NIX_PATH=nixpkgs=$nixpkgs

cat > /etc/nix/nix.conf << EOF
# default from nixos image
sandbox = false

# Set default store path to store in cached folder
# this will force "nix run" command to be executed inside chroot
store = $store

# max-jobs is about the number of derivations that Nix will build in parallel
max-jobs = auto

# cores is about parallelism inside a derivation, e.g. what make -j will use
# 0 means use all available cores
cores = 0
EOF
